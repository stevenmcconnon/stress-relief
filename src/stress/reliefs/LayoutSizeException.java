package stress.reliefs;


@SuppressWarnings("serial")
public class LayoutSizeException extends RuntimeException {
	
	String error;

	public LayoutSizeException() {
		error = "Unknown Error! Fix it bitch!";
	}

	public LayoutSizeException(String detailMessage) {
		super(detailMessage);
		error = detailMessage;
	}

	public LayoutSizeException(Throwable throwable) {
		super(throwable);
	}

	public LayoutSizeException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

}
