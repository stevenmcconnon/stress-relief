package stress.reliefs;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.FloatMath;
import android.util.Log;


/**
 * This class handles everything to do with the bitmaps of the game
 * @author stevenmcconnon
 *
 */
public class Bitmaps {

	public Bitmap bg, fg, mManSprite;
	private Resources resource;
	private static int layoutWidth = 0;
	private static int layoutHeight = 0;



	/**
	 * The constructor takes a resource from the surfaceview
	 * @param resource
	 */
	public Bitmaps(Resources resource) {
		this.resource = resource;
	}

	
	/**
	 * This loads the bitmaps needed in level1. It automatically sets them to the right size.
	 * @throws LayoutSizeException throws an error if the layoutSize isn't set
	 */
	public void loadGun () throws LayoutSizeException {
		if(layoutHeight == 0 && layoutWidth == 0)
			throw new LayoutSizeException("The Layout size is not set. Call setLayoutSize method before loadLevel1.");
		
		//load then resize the images
		bg = BitmapFactory.decodeResource(resource, R.drawable.bullethole);
		bg = resizeBitmap(bg, 0, layoutHeight);
		
	}
	
	
	
	
	/**
	 * An easy way to rescale any bitmap
	 * @param img
	 * @param newWidth set to 0 if unknown
	 * @param newHeight set to 0 if unknown
	 * @return returns a rescaled bitmap
	 */
	public Bitmap resizeBitmap (Bitmap img, int newWidth, int newHeight) {
		float ratio = 0;
		
		
		if (newWidth == 0) {
			ratio = (float)newHeight / img.getHeight();	
			
			if(ratio > 1)
				ratio = 1/ratio;
			
			newWidth = (int)( FloatMath.ceil(ratio) * img.getWidth() );
		}
		
		if (newHeight == 0) {
			ratio = (float)newWidth / img.getWidth();
			
			if(ratio > 1)
				ratio = 1/ratio;
			
			newHeight = (int)( FloatMath.ceil(ratio) * img.getHeight() );
		}
					
		return Bitmap.createScaledBitmap(img, newWidth, newHeight, true);
	}
	
	

	
	
	/**
	 * An easy way to rescale any bitmap sprite sheet. For spriteWidth or spriteHeight, 
	 * specify the height you want just one of the sprites to be, not the whole sheet
	 * 
	 * @param img
	 * @param newWidth set to 0 if unknown
	 * @param newHeight set to 0 if unknown
	 * @return returns a rescaled bitmap
	 */
	public Bitmap resizeSpriteSheet (Bitmap img, int spriteWidth, int spriteHeight, int rows, int cols) {
		//screen size is 800 x 480
		float ratio = 0;
		
		
		if (spriteWidth == 0) {
			spriteHeight = spriteHeight * rows;
			ratio = (float)spriteHeight / img.getHeight();
			
			if(ratio > 1)
				ratio = 1/ratio;
			
			spriteWidth = (int) FloatMath.ceil((ratio) * img.getWidth() );	
		}
		
		if (spriteHeight == 0) {
			spriteWidth = spriteWidth * cols;
			ratio = (float)spriteWidth / img.getWidth();
			
			if(ratio > 1)
				ratio = 1/ratio;
			
			spriteHeight = (int) FloatMath.ceil( (ratio) * img.getHeight() );
		}
		
		
					
		//Log.v("tag", "NewWidth: " + spriteWidth + " newHeight: " + spriteHeight);
		return Bitmap.createScaledBitmap(img, spriteWidth, spriteHeight, true);
	
		
	}
	
	
	
	
	
	/**
	 * Must be called before loading any levels.
	 * @param width the width of the screen
	 * @param height the height of the screen
	 */
	public static void setLayoutSize(int width, int height) {
		layoutWidth = width;
		layoutHeight = height;
	}
	
	
	/**
	 * 
	 * @return the width Of the Screen
	 */
	public static int getLayoutWidth() {
		return layoutWidth;
	}
	
	
	/**
	 * 
	 * @return the width of the screen
	 */
	public static int getLayoutHeight() {
		return layoutHeight;
	}
	
}
